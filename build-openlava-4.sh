#!/bin/bash

apt-get update -y
apt-get install -y build-essential libncurses-dev tcl-dev autoconf automake
patch --strip=1 --forward < lim.patch
aclocal
automake
./configure --prefix=/usr/local
make -j
make install